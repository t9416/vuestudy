import express from 'express'
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'TCz' });
});

router.get('/hi', function(req, res, next) {
  res.json({"hi":"bye"})
})

module.exports = router;
