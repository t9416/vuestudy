export default class TAction{
    constructor({ title = undefined } = {}) {
        this.title = title;
        this.enable = title === undefined ? false : true;
    }
}