import { ObjectUtils } from "@/utils";

export default class TBase {
    constructor( {id, uuKey = null, deleteFlag = false, createdBy, createdAt, modifiedBy, modifiedAt } = {}) {
        this.id = id;
        this.uuKey = uuKey === null ? ObjectUtils.generateUUID() : uuKey;
        this.deleteFlag = deleteFlag;
        this.createdBy = createdBy;
        this.createdAt = createdAt;
        this.modifiedBy = modifiedBy;
        this.modifiedAt = modifiedAt;
    }
}