import TBase from "@/components/Common/classes/TBase";
import TAction from "@/components/Common/classes/TAction";

export default class TListItem extends TBase {
    constructor({ type, imageUrl, badge = null, title, content, action = null, ...others} = {}) {
        super(others);
        this.type = type;
        this.component = this.setComponent(this.type);
        this.imageUrl = imageUrl;
        this.badge = badge;
        this.title = title;
        this.content = content;
        this.action = action === null ? new TAction() : new TAction(action);
    }

    setComponent( type ) {
        switch( type ) {
            case "BASIC": return () => import(`../TList/TBasicList`);
        }
    }
}