import VueRouter from "vue-router";
import Home from "@/pages/Home";
import Board from "@/pages/Board";
import Calendar from "@/pages/Calendar";
import MyPage from "@/pages/MyPage";
import PageNotFound from "@/pages/PageNotFound";
import Vue from "vue";

Vue.use(VueRouter);

// 2. 라우트를 정의하세요.
// Each route should map to a component. The "component" can
// 각 라우트는 반드시 컴포넌트와 매핑되어야 합니다.
// "component"는 `Vue.extend()`를 통해 만들어진
// 실제 컴포넌트 생성자이거나 컴포넌트 옵션 객체입니다.
const routes = [
    { path: '/', component: Home, name: `Home` },
    { path: '/board', component: Board, name: `Board` },
    { path: '/calendar', component: Calendar, name: `Calendar` },
    { path: '/my-page', component: MyPage, name: `MyPage` },
    { path: '*', component: PageNotFound, name: `404` }
]

// 3. `routes` 옵션과 함께 router 인스턴스를 만드세요.
// 추가 옵션을 여기서 전달해야합니다.
// 지금은 간단하게 유지하겠습니다.
const router = new VueRouter({
    mode: 'history',
    routes // `routes: routes`의 줄임
})

export default router;