import {compare} from "@/components/pages/Sort/common";

export default async array => {
    quick(array, 0, array.length - 1);
}


const quick = (array, left, right) => {
    const pivot = array[right];
    pivot.check = "pivot";

    let curArr = array.filter( (x, index) => (x.check === '') && index >= left && index <= right);
    while(curArr.length > 0) {
        curArr[0].check = 'check';
        const pivotIndex = array.indexOf(pivot);
        if(compare(pivot.value, curArr[0].value)) {
            array.splice(pivotIndex, 0, curArr[0]);
        } else {
            array.splice(pivotIndex+1, 0, curArr[0]);
        }

        curArr = array.filter( (x, index) => (x.check === '') && index >= left && index <= right);
    }

    pivot.check = 'finish';
    array.map( item => {
        if(item.check === 'check') item.check = ''
        return item;
    })

    const nonCheckArr = array.filter( (x, index) => (x.check === '') && index >= left && index <= right);
    if(nonCheckArr === 0) return;

    // const pivotIndex = array.indexOf(pivot);
    // if(pivotIndex === 0) quick(array, 1, array.length)
}