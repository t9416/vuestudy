export const compare = ( a, b ) => {
    const tA = !isNaN(parseFloat(a)) ? parseFloat(a) : a;
    const tB = !isNaN(parseFloat(b)) ? parseFloat(b) : b;
    return tA > tB;
}