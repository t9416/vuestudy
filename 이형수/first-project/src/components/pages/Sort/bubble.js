import {resolveSeconds} from "@/common/utils";
import {compare} from "@/components/pages/Sort/common";

export default async array => {
    for( let i = 0; i < array.length - 1; i++) {
        for( let j = 1; j < array.length - i; j++) {
            check(array, j, 'active');
            await resolveSeconds(0.2);
            if(compare(array[j-1].value, array[j].value)) {
                const temp = array[j]
                array[j] = array[j-1];
                array[j-1] = temp;

                check(array, j);
                check(array, j, 'active');
            }
            await resolveSeconds(0.2);
            check(array, j);
        }
        check(array, array.length - 1 - i, 'finish');
    }
    check(array, 0, 'finish');
}

const check = (array, index, check = '') => {
    if(check !== 'active' && check !== '' && check !== 'finish') return;
    array[index].check = check;
    if(check !== 'finish') array[index-1].check = check;
}