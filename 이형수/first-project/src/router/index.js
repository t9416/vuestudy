import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from "@/components/pages/Home";
import About from "@/components/pages/About";
import Poll from "@/components/pages/Poll";
import Sort from "@/components/pages/Sort";

const routes = [
    { path: '/', component: Home },
    { path: '/about/test', component: About },
    { path: '/poll', component: Poll },
    { path: '/sort', component: Sort }
]

Vue.use(VueRouter);

export default new VueRouter({
    routes
})