export const resolveSeconds = async n => {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve('resolved');
        }, n * 1000);
    });
}