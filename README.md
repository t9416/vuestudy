
# VueStudy
> 천재들이 모여 하는 Vue Study
  
  
  
### 💎 목표
- MVVM모델을 이해하여 기존에 알던 MVC모델과 다른 경험을 할 수 있다.
- SPA(Single Page Application)이 무엇인지 이해하며 Vue.js의 특징을 이해할 수 있다.
- Vue.js에 대한 자신감을 가질 수 있다.
- 최종적으로 실무에 적용할 수 있다.
  
  
---
  
  
### 📅 날짜
- 2022.04.12 ~ 진행중
  
  
  
### 📌 진행과제
1. **오리엔테이션** - 스터디 운영 방법과 앞으로의 진행 방향 (04.12)
2. **Vue.js란?** - Vue.js에 대한 개념과 이해 정리 (~04.19)
   1. Reactive
   2. Node.js
   3. Component
   4. NPM
   5. Life Cycle
   6. HTML
   7. DOM
   8. SPA
   9. Vue Router
3. **기본 예제 만들기** - 간단한 실습 (~04.27)
4. **Vue.js 프로젝트 구현한 예제 만들기** - 확장된 실습 (~05.04)
5. **Vue.js 프로젝트 구현한 예제 만들기2** (~05.11)
6. **VueStudy Project 논의** (~05.18)
7. **VueStudy Project 논의2** (~05.18)
8. **프로젝트 셋팅** (~05.24)
9. **프로젝트 DB 셋팅, 프로젝트 구조 고민** (~05.30)
  
  
---
  
  
### 🖥 내용

```
[1주차] 04.19

 1. Vue.js에 대해 각자 조사해온 내용 서로 나누면서 개념 이해하기
   - Vue.js의 컴포넌트와 반응형에 대해 이해한다면 사용자에게 편리한 UI/UX를 제공할 수 있다.
   - 가상 DOM을 이해한다면 다수의 사용자에 대응한 서비스의 성능을 기대할 수 있다.
 
 2. Vue.js 특징 이해하기
   - MVVM 패턴(모델 : 데이터베이스모델, 뷰 : HTML 마크업, 뷰-모델 : 뷰 인스턴스)
   - SPA(Single Page Application)으로 한 번의 로딩으로 단일 페이지에 동적으로 화면 포현
   - 리액티브한 프로그래밍으로 실행 모델에 대한 비동기 방식
   - 컴포넌트란, HTML에서 구성되는 단위이자 화면을 표현하는 블록
   - 가상 DOM을 활용하여 사용자와 앱이 지속적으로 상호작용 가능
 
 3. Vue.js 생명 주기 알아보기
   - Vue가 처음으로 인스턴스화하면 거치는 일련의 이벤트 
   - 과정 :
      인스턴스 생성(이벤트 생성/초기화) 
      > 템플릿과 가상 DOM 생성(템플릿 혹은 렘더링 함수를 찾아 컴파일, 가상 DOM을 만들고 그 결과를 HTML DOM에 마운트) 
      > 이벤트 루프(뷰-모델 데이터 변화에 따라 가상 DOM에 렌더링되어 HTML DOM에 적용) 
      > 인스턴스 소멸(관찰자, 이벤트 리스너, 자식 컴포넌트 등 삭제)
```
  
  
```
[2주차] 04.27

 1. Vue.js 배경과 Javascript의 흐름, 우리가 공부하면서 나아갈 방향성 고민
 2. 각자 만들어온 간단한 실습 예제 발표
 
```
  
  
```
[3주차] 05.04

 1. 동적으로 데이터를 바인딩하는 방법

 2. Router
   - <router-link to="주소"></router-link> : 링크 연결
   - <router-view></router-view> : 연결된 링크의 컴포넌트 

 3. Vue.js 페이지 구성 
   <template></template>
   <script></script>
   <style></style>

 4. template 특징 및 주의사항
    - 하나의 template에 하나의 큰 div 태그(혹은 다른 태그)로만 구성
    - v-for : 데이터가 담겨 있는 인스턴스를 활용하여 key(고유의 값)을 정의해 보여줌
      ex) <section v-for = "(변수명, 인덱스명) in 인스턴스명.변수명": key = "인덱스명">
    - {{ 인스턴스명.변수명 }} : script에서 정의한 내용을 html에서 보여줄 때 사용
    - import를 이용해서 데이터 파일 등을 넣음(단, import한 인스턴스를 사용하지 않을 시에도 오류 발생)
    - 값이 변할 때마다 watcher가 동적으로 html에 반영
    - 태그 이름을 사용자가 마음대로 정의하여 쓸 수 있음
      ex) <trycatch/>
    -  v-click = @click 동일한 표현
    - '@'를 이용하여 이벤트를 사용자가 마음대로 정의하여 쓸 수 있음 
      이 이벤트는 트리거에 의해 정의되고 실행되어야 함
      ex) @trycatch = "trycatch"
    - computed : {} 를 이용하여 기능을 변수를 선언하여 사용
    - props
    - emit
```

```
 [4주차] 05.11
    프로젝트 시작에 대한 논의
```


```
 [5주차] 05.18
 
 - 프로젝트 논의
    - Setting 설정하기
    - 서버(Node)
    - 토큰을 이용한 서버랑 UI 연결

 - 이름 목록 :
    -  파이널리(Finally), 익셉션(Exception) .. 등등

 - 주제 : 사내 SNS (커뮤니티)
    - 일정 관리
    - 생일 캘린더
    - 맛집 공유
    - 장보기 목록
    - 스터디 내용(보류)
    - 투표 기능
    - 회식 일정 공유
    - 점심 일정 공유
    - 화이트리스트, 블랙리스트
    - 그룹핑

 - 메뉴(카테고리)
    - (홈)
    - 일정 (캘린더) -> 생일, 스터디일정, 회식일정 (개인/공식)
    - 자유게시판 -> 전체, 그룹별, 투표, 장보기
    - 마이페이지 -> 친구목록, 그룹관리
```

```
 [6주차] 05.24
 
 1. 프로젝트 생성
    -  Vue create client > default(vue2)
    - 프로젝트 실행 명령어 : yarn 
    
 2. Vue Router 설치 
	- yarn add vue-router@3
	
 3. 프로젝트 구성 만들기 
 
 4. Express 설치
    - Npm install express-generater -g
    - Express server
    - Yarn install 
    - 실행명령어 : yarn start
 
 5. Client에 axios 설치
    - Yarn install axis
 
 6. Express cors 설정
    - Yarn add cars

```