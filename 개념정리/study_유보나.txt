Vue js란 ?

1. Reactive
- 반응형(Reactive) ........

2. Node.js
- 확장성이 있는 네트워크 애플리케이션(특히 서버 사이드) 개발에 사용되는 소프트웨어 플랫폼

3. Component
- 프로그래밍에 있어 재사용이 가능한 각각의 독립된 모듈
  컴포넌트 기반 프로그래밍을 하면 마치 레고 블록처럼 이미 만들어진 컴포넌들을 조합하여 화면을 구성할 수 있음

4. NPM
- 노드 패키지 매니저
  앱 의존성 관리를 위해 사용, 터미널 창에 다음 명령어로 최신버전으로 업그레이드 가능(mac)

5. Life Cycle
- 모든 Vue 앱은 Vue 함수로 새 Vue 인스턴스를 만드는 것

6. HTML
- Hyper Text Mark-up Language의 약자로 웹 페이지를 위한 지배적인 마크업 언어
  HTML은 제목, 단락, 목록 등과 같은 본문을 위한 구조적 의미를 나타내는 것뿐만 아니라 링크, 인용과 그 밖의 항목으로 구조적 문서를
  만들 수 있는 방법을 제공

7. DOM
- 가상 돔(Virtual DOM)이라는 개념을 사용해 SPA를 구현

8. SPA
- SPA(Single Page Application)
  싱글 페이지 애플리케이션(Single Page Application, SPA)은 서버로부터 새로운 페이지를 불러오지 않고 현재의 페이지를 동적으로 다시 작성함으로써 사용자와 소통하는 웹 애플리케이션이나 웹사이트를 말함.
  SPA는 하나의 HTML 파일을 가지고 나머지는 자바스크립트를 이용해서 동적으로 화면을 구성할 수 있음

9. Vue Router
- Vue 라우터는 Vue.js의 공식 라우터로, Vue.js를 사용한 싱글 페이지 앱(SPA)을 쉽게 만들 수 있도록 Vue.js의 코어와 긴밀히 통합